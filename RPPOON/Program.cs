﻿using LV1;
using System;
using System.Collections.Generic;

namespace RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note();
            Note note2 = new Note("Do homework", "Ana", 2);
            Note note3 = new Note(ref note2);
            note1.setText("Do dishes");
            note1.Priority = 3;

            DateTimeNote dateTimeNote1 = new DateTimeNote();
            dateTimeNote1.setTime(2010, 3, 21, 12, 12, 12);
            DateTimeNote dateTimeNote2 = new DateTimeNote(ref dateTimeNote1);
            DateTimeNote dateTimeNote3 = new DateTimeNote(ref note1);
            DateTimeNote dateTimeNote4 = new DateTimeNote(ref note2);

            ToDo ToDoList = new ToDo();

            string text;
            string name;
            int priority;

            ToDoList.Add(ref dateTimeNote1);
            ToDoList.Add(ref dateTimeNote2);
            ToDoList.Add(ref dateTimeNote3);
            ToDoList.Add(ref dateTimeNote4);

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Enter a note: ");
                text = Console.ReadLine();
                Console.WriteLine("Enter a name: ");
                name = Console.ReadLine();
                Console.WriteLine("Enter a priority of a note: ");
                priority = Convert.ToInt32(Console.ReadLine());
                ToDoList.Add(text, name, priority);
            }


            Console.WriteLine("Ispis prije izvrsavanja zadataka:\n");
            Console.WriteLine(ToDoList.ToString());
            ToDoList.RemoveMaxPriority();
            Console.WriteLine("Ispis nakon izvrsavanja zadataka:\n");
            Console.WriteLine(ToDoList.ToString());

        }
    }
}
