﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON
{
    class Note
    {
 
        private String text;
        private String author;
        private int priority;

        
        public string getText() { return this.text; }
        public string getAuthor() { return this.author; }
        public int getPriority() { return this.priority; }

        public void setText(string text) { this.text = text; }
        public void setPriority(int priority) { this.priority = priority; }
        private void setAuthor(string author) { this.author = author; }
        

        public string Text { get => text; set => text = value; }
        public string Author { get => author; private set => author = value; }
        public int Priority { get => priority; set => priority = value; }

        public Note()
        {
            this.text = "Text";
            this.author = "Author";
            this.priority = 1;
        }
        public Note(string text,string author, int priority)
        {
            this.text = text;
            this.author = author;
            this.priority = priority;
        }
        public Note(ref Note note1)
        {
            this.text = note1.text;
            this.author = note1.author;
            this.priority = note1.priority;
        }
        public override string ToString()
        {
            return "Note: "+text+"\nAuthor: "+author+"\n";
        }

    }
    class DateTimeNote: Note
    {
        private DateTime time;

        public DateTime Time { get => time; set => time = value; }

        public void setTime()
        {
            this.time = DateTime.Now;
        }
        public void setTime(int year, int month,int day,int hour, int minute, int second)
        {
            this.time = new DateTime(year, month, day, hour, minute, second);
        }

        public DateTimeNote() : base()
        {
            this.time = DateTime.Now;
        }
        public DateTimeNote(string text, string author, int priority) : base(text, author, priority)
        {
            this.time = DateTime.Now;
        }
        public DateTimeNote(ref Note note1): base(note1.Text,note1.Author, note1.Priority)
        {
            this.time = DateTime.Now;
        }
        public DateTimeNote(string text, string author, int priority, int year, int month, int day, int hour, int minute, int second):base(text, author, priority)
        {
            this.time = new DateTime(year, month, day, hour, minute, second);
        }
        public DateTimeNote(ref DateTimeNote note1): base(note1.Text, note1.Author, note1.Priority)
        {
            this.time = note1.time; 
        }

        public override string ToString()
        {
            return base.ToString()+time.ToString()+"\n";
        }
    }
}
