﻿using RPPOON;
using System;
using System.Collections.Generic;
using System.Text;

namespace LV1
{
    class ToDo
    {
        private List<DateTimeNote> notes;

        internal List<DateTimeNote> Notes { get => notes; set => notes = value; }

        public ToDo()
        {
            Notes = new List<DateTimeNote>();
        }
        public int Add(string text,string author, int priority)
        {
            DateTimeNote note = new DateTimeNote(text, author, priority);
            Notes.Add(note);
            return Notes.IndexOf(note);
        }
        public int Add(string text, string author, int priority, int year, int month, int day, int hour, int minute, int second)
        {
            DateTimeNote note = new DateTimeNote(text, author, priority, year, month, day, hour, minute, second);
            Notes.Add(note);
            return Notes.IndexOf(note);
        }
        public int Add(ref DateTimeNote note)
        {
            Notes.Add(note);
            return Notes.IndexOf(note);
        }

        public void Remove(int index)
        {
            Notes.RemoveAt(index);
        }

        public DateTimeNote GetDateTimeNote(int index)
        {
            return Notes[index];
        }

        public override string ToString()
        {
            StringBuilder stringNotes = new StringBuilder();
            foreach(DateTimeNote note in Notes)
            {
                stringNotes.Append(note.ToString()).Append("\n");
            }
            return stringNotes.ToString();
        }

        private int GetMaxPriority()
        {
            int priority = 0;
            if (Notes.Count > 0)
            {
                priority = Notes[0].Priority;
            }
            foreach(DateTimeNote note in Notes)
            {
                if (priority < note.Priority)
                {
                    priority = note.Priority;
                }
            }
            return priority;
        }

        public void RemoveMaxPriority()
        {
            int priority = GetMaxPriority();
            for(int i = 0; i < Notes.Count; i++)
            {
                if (priority == Notes[i].Priority)
                {
                    Remove(i);
                    i--;
                }
            }
        }

    }
}
